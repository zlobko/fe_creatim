import Vue from 'vue'
import VueRouter from 'vue-router'

import './plugins/vuetify'
import App from './App.vue'
import Index from './components/Index.vue'
import Index2 from './components/Index2.vue'
import Index3 from './components/Index3.vue'
import Index4 from './components/Index4.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueRouter)

Vue.use(VueAxios, axios)
Vue.config.productionTip = false

//define routs.
const routes = [
  { path: '/', component: Index },
  { path: '/primer2', component: Index2 },
  { path: '/primer3', component: Index3 },
  { path: '/primer4', component: Index4 }
];

//set up router
const router = new VueRouter({
  routes: routes,
  mode: 'history'  //remove /#/ from navigation  
})

new Vue({
  render: h => h(App),
  router,
}).$mount('#app')
